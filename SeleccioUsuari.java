import javax.swing.*;
import java.awt.*;
import javax.swing.event.*;
import java.awt.event.*;

class SeleccioUsuari{
	private JFrame usuariFrame = new JFrame("Login");
	private JPanel panusuari = (JPanel)usuariFrame.getContentPane();
	private JTextField eUsuari = new JTextField();
	private JButton bUsuari = new JButton(new ImageIcon("img/send.png"));
	private String usuari;
	
	public SeleccioUsuari(){
		usuariFrame.setSize(400, 210);//Que es puguessin crear usuaris i guardar-los a sa bd i comparar es hash de sa contrasenya a sa bd
		usuariFrame.setLayout(new GridBagLayout());
		usuariFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		JLabel lUsuari = new JLabel("Quin es el teu nick?");
		lUsuari.setFont(new Font("Comic Sans MS", Font.BOLD, 24));
		eUsuari.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		bUsuari.setFocusPainted(false);
		bUsuari.setBorder(BorderFactory.createEmptyBorder());//Llev bordes
		bUsuari.setBackground(new Color(238, 238, 238));
		JLabel imgUsuari = new JLabel(new ImageIcon("img/user_icono_res.png"));
		
		JPanel panTitol = new JPanel(new FlowLayout(FlowLayout.LEFT));
		panTitol.add(imgUsuari);
		panTitol.add(lUsuari);
		
		JPanel panTot = new JPanel(new GridBagLayout());
		panTot.setBackground(new Color(238, 238, 238));
		panusuari.setBackground(new Color(210, 210, 210));
		
		panTot.add(panTitol, new GridBagConstraints(0, 1, 2, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
		panTot.add(eUsuari, new GridBagConstraints(0, 2, 1, 1, 0.5, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 40, 0));
		panTot.add(bUsuari, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(10, 10, 10, 10), 0, 0));
		
		panusuari.add(panTot,new GridBagConstraints(0, 0, 1, 1, 0.1, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(8, 8, 8, 8), 0, 0));
		
		eUsuari.addKeyListener(new KeyAdapter(){
			public void keyReleased(KeyEvent e){
				//Si sa tecla pitjada es intro i esta ben format insertar tirada
				if(e.getExtendedKeyCode() == 10){
					if(!eUsuari.getText().isEmpty()){
						setUsuari();
					}
				}
			}
		});
		
		bUsuari.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				setUsuari();
			}
		});
		
		usuariFrame.setLocationRelativeTo(null);
		usuariFrame.setVisible(true);
	}
	private void setUsuari(){
		if(usuariBenFormat(eUsuari.getText())){//Longitud maxima de 50 i no permeti '' per sql injection
			usuari = eUsuari.getText().trim();
			usuariFrame.setVisible(false);
		}
	}
	public String getUsuari(){
		return usuari;
	}
	private boolean usuariBenFormat(String con){
		if(con.contains("'")){
			missatgeError("No pot contenir comilles");
			return false;
		}
		if(con.length() > 50){
			missatgeError("El nom de l'usuari no pot tenir mes de 50 caràcters");
			return false;
		}
		return true;
	}
	private void missatgeError(String msg){
		JOptionPane.showMessageDialog(null, msg, "Error en l'entrada de l'usuari", JOptionPane.ERROR_MESSAGE);
	}
}

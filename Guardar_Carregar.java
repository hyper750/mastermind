import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.sql.*;

interface Arxiu{
	public File arxiuPartides = new File("Partides.obj");
}
interface GuardarPartida{
	public boolean guardar(Partida partida);
	public void close();
}
class GuardarPartidaFitxer implements GuardarPartida, Arxiu{
	private ObjectOutputStream sortidaPartida = null;
	public GuardarPartidaFitxer(){
		try{
			sortidaPartida = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(arxiuPartides)));
		}
		catch(FileNotFoundException e){
			System.err.println("Arxiu no trobat");
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	public boolean guardar(Partida partida){
		//Guardar partida a un fitxer
		try{
			sortidaPartida.writeObject(partida);
			sortidaPartida.flush();
		}
		catch(IOException e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public void close(){
		try{
			sortidaPartida.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}/*Necessit es close a nes fitxer pero a bd no però com que a nes singleton es declarat com a s'interficie GuardarPartida pues el tenc que implementar deixant-lo buit a nes de bd*/
	public String toString(){
		return "Guardant partides al fitxer...";
	}
}

class GuardarPartidaBD implements GuardarPartida{
	public static void mostraSQLException(SQLException ex){
		ex.printStackTrace(System.err);
		System.err.println("SQLState: " + ex.getSQLState());
		System.err.println("Error Code: " + ex.getErrorCode());
		System.err.println("Message: " + ex.getMessage());
		Throwable t = ex.getCause();
		while(t != null){
			System.out.println("Cause: " + t);
			t = t.getCause();
		}
	}
	public void close(/*Esta buit pero ho necessit a nes de guardarpartidafitxer i com que esta creat a partir de s'interficie pues necessit crear-lo, el crei de s'interficie perque m'el detectu desde el singleton*/){}
	public boolean guardar(Partida partida){
		//Guardar partida a la base de dades
		String url = "jdbc:mysql://localhost:3306/mastermind";
		String usuari = "mastermind";
		String pwd = "123Cicle123";
		String cad0 = "use mastermind";
		//~ String insertar = null;
		//~ String update = null;
		//~ String countPartides = "select count(*) from partides where id='" + partida.getId() + "' and usuari='" + partida.getUsuari() + "'";
		String insertarPartida = "call insertarpartida('" + partida.getId() + "', '" + partida.getUsuari() + "', '" + partida.getHora().getHora() + "', '" + partida.getHora().getMinuts() + "', '" + partida.getHora().getSegons() + "', '" + partida.getHora().getMilisegons() + "', '" + partida.getDificultat() + "', '" + partida.getNumCpu() + "', '" + ((partida.getAcabada())? "1" : "0") + "')";
		String countTirades = "select count(*) from tirades where idPartida='" + partida.getId() + "' and usuari='" + partida.getUsuari() + "'";
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		String insertarTirada = null;
		Tirada t = null;
		/*if(partida.getAcabada()){
			insertar = "insert into partides values('" + partida.getId() + "', '" + partida.getUsuari() + "', '" + partida.getHora().getHora() + "', '" + partida.getHora().getMinuts() + "', '" + partida.getHora().getSegons() + "', '" + partida.getHora().getMilisegons() + "', '" + partida.getDificultat() + "', '" + partida.getNumCpu() + "', 1)";
			update = "update partides set hora='" + partida.getHora().getHora() + "', minut='" + partida.getHora().getMinuts() + "', segon='" + partida.getHora().getSegons() + "', milisegon='" + partida.getHora().getMilisegons() + "', acabada='1' where id='" + partida.getId() + "' and usuari='" + partida.getUsuari() + "'";//Actualitzar temps, si esta acabada i ses tirades
		}
		else{
			insertar = "insert into partides values('" + partida.getId() + "', '" + partida.getUsuari() + "', '" + partida.getHora().getHora() + "', '" + partida.getHora().getMinuts() + "', '" + partida.getHora().getSegons() + "', '" + partida.getHora().getMilisegons() + "', '" + partida.getDificultat() + "', '" + partida.getNumCpu() + "', 0)";
			update = "update partides set hora='" + partida.getHora().getHora() + "', minut='" + partida.getHora().getMinuts() + "', segon='" + partida.getHora().getSegons() + "', milisegon='" + partida.getHora().getMilisegons() + "', acabada='0' where id='" + partida.getId() + "' and usuari='" + partida.getUsuari() + "'";//Actualitzar temps, si esta acabada i ses tirades
		}*/
		//~ String insertarPartides = null;
		//Si sa partida amb aquest id existeix actualitzar informacio sino crear-la
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(url, usuari, pwd);
			st = con.createStatement();
			
			st.executeUpdate(cad0);
			/*rs = st.executeQuery(countPartides);
			rs.next();
			if(rs.getInt(1) == 0){//Si sa partida existeix a sa BD fer un update sino un insert
				st.executeUpdate(insertar);
				//~ System.out.println("no existeix");
			}
			else{
				//~ System.out.println(partida.getUsuari());
				st.executeUpdate(update);
				//~ System.out.println("existeix");
			}*/
			st.executeUpdate(insertarPartida);
			rs = st.executeQuery(countTirades);
			rs.next();
			//Guardant tirades
			for(int x = rs.getInt(1); x < partida.getIntents(); x++){//Guard ses tirades que no hi ha la BD
				t = partida.getTirada(x);
				insertarTirada = "insert into tirades values(null, '" + partida.getUsuari() + "', '" + partida.getId() + "', '" + t.getNumUsuari() + "', '" + t.getNumMalString() + "', '" + t.getMalColocats() + "', '" + t.getBenColocats() + "')";
				st.executeUpdate(insertarTirada);
				//~ System.out.println(t);
			}
		}
		catch(ClassNotFoundException e){
			e.printStackTrace();
		}
		catch(SQLException e){
			mostraSQLException(e);
		}
		finally{
			try{
				if(con != null) con.close();
			}
			catch(SQLException e){
				e.printStackTrace();
			}
			
			try{
				if(st != null) con.close();
			}
			catch(SQLException e){
				mostraSQLException(e);
			}
		}
		return true;
	}
	public String toString(){
		return "Guardant partides a la Base de Dades...";
	}
}
interface CarregarPartida{
	public ArrayList<Partida> carregar(String usuari);
}
class CarregarPartidaFitxer implements CarregarPartida, Arxiu{
	public ArrayList<Partida> carregar(String usuari){
		ArrayList<Partida> p = new ArrayList<Partida>();
		if(Joc.arxiuPartides.exists()){
			ObjectInputStream ois = null;
			try{
				ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(arxiuPartides)));
				while(true){
					p.add((Partida)ois.readObject());
				}
			}
			catch(FileNotFoundException e){
				e.printStackTrace();
			}
			catch(ClassNotFoundException e){
				e.printStackTrace();
			}
			catch(EOFException e){
				try{
					if(ois != null) ois.close();
				}
				catch(IOException a){
					a.printStackTrace();
				}
			}
			catch(IOException e){
				e.printStackTrace();
			}
		}
		return p;
	}
	public String toString(){
		return "\n\nCarregant Partides del fitxer...";
	}
}
class CarregarPartidaBD implements CarregarPartida{
	public static void mostraSQLException(SQLException ex){
		ex.printStackTrace(System.err);
		System.err.println("SQLState: " + ex.getSQLState());
		System.err.println("Error Code: " + ex.getErrorCode());
		System.err.println("Message: " + ex.getMessage());
		Throwable t = ex.getCause();
		while(t != null){
			System.out.println("Cause: " + t);
			t = t.getCause();
		}
	}
	public ArrayList<Partida> carregar(String usuari){
		ArrayList<Partida> partides = new ArrayList<Partida>();
		Connection con = null;
		Statement st = null;
		Statement st1 = null;
		ResultSet rs = null;
		ResultSet rsTirades = null;
		String url = "jdbc:mysql://localhost:3306/mastermind";
		String user = "mastermind";
		String pwd = "123Cicle123";
		String cad0 = "use mastermind";
		String cad1 = "select * from partides where usuari='" + usuari + "'";
		String cad2 = null;
		int id, dificultat, hora, minut, segon, ms;
		String numCpu;
		boolean acabada;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(url, user, pwd);
			st = con.createStatement();
			st1 = con.createStatement();
			st.executeUpdate(cad0);
			rs = st.executeQuery(cad1);
			while(rs.next()){
				id = rs.getInt(1);
				hora = rs.getInt(3);
				minut = rs.getInt(4);
				segon = rs.getInt(5);
				ms = rs.getInt(6);
				dificultat = rs.getByte(7);
				numCpu = rs.getString(8);
				acabada = ((rs.getByte(9) == 0)? false : true);
				partides.add(new Partida(id, numCpu, dificultat, hora, minut, segon, ms, usuari, acabada));
				cad2 = "select * from tirades where idPartida='" + id + "' and usuari='" + usuari + "'";
				rsTirades = st1.executeQuery(cad2);
				while(rsTirades.next()){//String numUsuari, String numMal, byte malColocats, byte benColocats
					partides.get(partides.size()-1).afegirTirada(rsTirades.getString(4), rsTirades.getString(5), rsTirades.getByte(6), rsTirades.getByte(7));
				}
			}
		}
		catch(ClassNotFoundException e){
			e.printStackTrace();
		}
		catch(SQLException e){
			mostraSQLException(e);
		}
		return partides;
	}
	public String toString(){
		return "Carregant Partides de la Base de Dades...";
	}
}
class Singleton{
	private GuardarPartida magatzemGuardar;
	private CarregarPartida magatzemCarregar;
	private static Singleton INSTANCIA = new Singleton();
	private Singleton(){
		codiCarregar(1);
	}
	public static Singleton getInstancia(){
		return INSTANCIA;
	}
	public GuardarPartida getMagatzemGuardar(){
		return magatzemGuardar;
	}
	public CarregarPartida getMagatzemCarregar(){
		return magatzemCarregar;
	}
	public void codiGuardar(int codi){
		switch(codi){
			case 1: magatzemGuardar = new GuardarPartidaFitxer(); break;
			case 2: magatzemGuardar = new GuardarPartidaBD(); break;
			default: magatzemGuardar = new GuardarPartidaFitxer(); break;
		}
	}
	public void codiCarregar(int codi){
		switch(codi){
			case 1: magatzemCarregar = new CarregarPartidaFitxer(); break;
			case 2: magatzemCarregar = new CarregarPartidaBD(); break;
			default: magatzemCarregar = new CarregarPartidaFitxer(); break;
		}
	}
}

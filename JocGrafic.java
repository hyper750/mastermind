import javax.swing.*;
import java.awt.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.table.*;

public class JocGrafic{
	private JFrame mainFrame = new JFrame("Mastermind");
	private JPanel pan = (JPanel)mainFrame.getContentPane();
	private JList<Partida> llistaPartides; 
	private JTable taulaTirades;//
	private Joc objJoc = null;
	private Partida partidaAnterior = null;
	private JCheckBoxMenuItem menuOnline = null;//
	private DefaultListModel<Partida> modelPartides = null;
	private JTextField tEntrada = null;
	private JButton bEntrada = null;
	private DefaultTableModel modelTirades = null;
	private JScrollPane panScrollPartides = null;
	private JScrollPane panScrollTirades = null;
	private Ranking ranking = new Ranking();
	private SeleccioUsuari frameUsuari = new SeleccioUsuari();
	
	public JocGrafic(){
		mainFrame.setSize(1200, 700);//Tenia 1100 pero no hi cabia tot
		mainFrame.setLayout(new GridBagLayout());
		mainFrame.addWindowListener(new TancarMasterMind());
		pan.setBackground(new Color(210, 210, 210));
		
		//Barra menu
		JMenuBar menu = new JMenuBar();
		
		JMenu menuArxiu = new JMenu("Arxiu");
		JMenu menuNovaPartida = new JMenu("Nova partida");
		JMenuItem menuNovaAmbAjuda = new JMenuItem("Nova partida amb ajuda");
		JMenuItem menuNovaSenseAjuda = new JMenuItem("Nova partida sense ajuda");
		menuOnline = new JCheckBoxMenuItem("Partides online");		
		JMenuItem rankingOnline = new JMenuItem("Ranking online");
		JMenuItem borrarPartides = new JMenuItem("Borrar partides locals");
		menuNovaPartida.add(menuNovaSenseAjuda);
		menuNovaPartida.add(menuNovaAmbAjuda);
		menuArxiu.add(menuNovaPartida);
		menuArxiu.add(borrarPartides);
		menuArxiu.add(rankingOnline);
		menuArxiu.add(menuOnline);
		
		menu.add(menuArxiu);
		mainFrame.setJMenuBar(menu);
		
		
		//Titol
		JLabel ltitol = new JLabel("MASTERMIND");
		ltitol.setFont(new Font("Comic Sans MS", Font.PLAIN, 32));
		JPanel panTitol = new JPanel(new GridBagLayout());
		//~ panTitol.add();
		panTitol.add(ltitol, new GridBagConstraints(0, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(10, 10, 10, 10), 0, 0));
		
		//Titol amb nom usuari
		JLabel nomUsuari = new JLabel();
		nomUsuari.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		
		//PART ESQUERRA DES LLISTAT DE PARTIDES
		modelPartides = new DefaultListModel<Partida>();
		llistaPartides = new JList<Partida>(modelPartides);
		llistaPartides.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JLabel lPartides = new JLabel("Partides");
		lPartides.setHorizontalAlignment(SwingConstants.CENTER);
		
		
		
		panScrollPartides = new JScrollPane(llistaPartides);
		JPanel panPartides = new JPanel(new GridBagLayout());
		panPartides.setBackground(new Color(238, 238, 238));
		panPartides.add(lPartides, new GridBagConstraints(0, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(8, 0, 0, 0), 0, 0));
		panPartides.add(panScrollPartides, new GridBagConstraints(0, 1, 1, 1, 0.1, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(8, 16, 16, 16), 0, 0));
		
		//PART DRETA DES LLISTAT DE TIRADES
		String[] nomColumnes = {"Numero", "Ben posicionats", "Mal posicionats", "Ajuda"};
		modelTirades = new DefaultTableModel(null, nomColumnes){
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};//Perque no es puguin modificar ses cel·les des jtable
		taulaTirades = new JTable(modelTirades);
		taulaTirades.setRowHeight(30);
		DefaultTableCellRenderer centreRender = new DefaultTableCellRenderer();
		centreRender.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
		for(int x = 0; x <= 3; x++){
			taulaTirades.getColumn(taulaTirades.getColumnName(x)).setCellRenderer(centreRender);
		}
		JLabel ltirades = new JLabel("Tirades");
		taulaTirades.getTableHeader().setReorderingAllowed(false);
		
		ltirades.setHorizontalAlignment(SwingConstants.CENTER);
		JPanel panTirades = new JPanel(new GridBagLayout());
		panTirades.setBackground(new Color(238, 238, 238));
		panScrollTirades = new JScrollPane(taulaTirades);
		panTirades.add(ltirades, new GridBagConstraints(0, 0, 2, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(8, 0, 0, 0), 0, 0));
		panTirades.add(panScrollTirades, new GridBagConstraints(0, 1, 2, 1, 0.1, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(8, 16, 16, 16), 0, 0));
		
		//Entrada de text
		tEntrada = new JTextField();
		tEntrada.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		bEntrada = new JButton(new ImageIcon("img/send.png"));
		bEntrada.setFocusPainted(false);
		bEntrada.setBorder(BorderFactory.createEmptyBorder());//Llev bordes
		bEntrada.setBackground(new Color(238, 238, 238));//Pos es fondu des boto des mateix color que es fondu des panel i no es veu gg wp
		//~ JPanel panEntrada = new JPanel(new GridBagLayout());
		//~ panEntrada.setBackground(new Color(238, 238, 238));
		//~ panEntrada.add(tEntrada, new GridBagConstraints(0, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 820, 0));
		//~ panEntrada.add(bEntrada, new GridBagConstraints(1, 0, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 0, 10, 10), 0, 0));
		panTirades.add(tEntrada, new GridBagConstraints(0, 2, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(8, 16, 16, 0), 0, 0));
		panTirades.add(bEntrada, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(8, 16, 16, 16), 0, 0));
		
		while(!(frameUsuari.getUsuari() != null)){//Se me quedi esperant fins que no li entri es nom d'usuari
			try{
				Thread.sleep(500);
			}
			catch(InterruptedException ie){
				ie.printStackTrace();
			}
		}
		objJoc = new Joc(frameUsuari.getUsuari());
		for(Iterator it = objJoc.iterator(); it.hasNext();){
			Partida p1 = (Partida)it.next();
			if(p1.getUsuari().equals(frameUsuari.getUsuari())){
				modelPartides.addElement(p1);
			}
		}
		if(modelPartides.getSize() == 0){
			tEntrada.setEnabled(false);
			bEntrada.setEnabled(false);
		}
		nomUsuari.setText(frameUsuari.getUsuari());
		 /* GridBagConstraints(
		 * int gridx, //Numero de columna on comença
		 * int gridy, //Numero de línia on comença
		 * int gridwith, //Cel·les en horizontal que ocupa
		 * int gridheight, //Cel·les en vertical que ocupa
		 * double weightx, //Estirar fila de la cel·la
		 * double weighty, //Estirar columna de la cel·la
		 * int anchor, (GridBagConstraints.CENTER) //Situa comp. dins cel·la
		 * int fill, (GridBagConstraints.BOTH) //Estirar comp. dins cel·la
		 * Insets insets, //(a, b, c, d) farcit extern a la casella
		 * int ipadx, //farcit intern de la casella
		 * int pady //farcit intern de la casella
		 * 
		 *)*/ 
		 
		//Panell primari des jframe
		pan.add(panTitol, new GridBagConstraints(0, 0, 2, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
		pan.add(panPartides, new GridBagConstraints(0, 1, 1, 1, 0.1, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 10, 0, 0), 0, 0));
		pan.add(panTirades, new GridBagConstraints(1, 1, 1, 1, 0.1, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 10, 0, 10), 0, 0));
		pan.add(nomUsuari, new GridBagConstraints(0, 2, 1, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 10, 5, 0), 0, 0));
		//~ pan.add(panEntrada, new GridBagConstraints(0, 2, 0, 1, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
		
		
		//Quant clic a un item des sa llista surtin ses tirades de sa partida selecciona
		llistaPartides.addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent le){
				if (!le.getValueIsAdjusting()) {//Perque no es repetesqui quant pitjes	
					Partida p1 = llistaPartides.getSelectedValue();
					if(p1 != null){
						if(!(p1.getAcabada())){//Si sa partida no esta acabada començar a calcular
							p1.reanudarPartida();
						}
						Tirada t1;
						//Lev totes tirades
						for(int i = modelTirades.getRowCount()-1; i >= 0; i-- ){
							modelTirades.removeRow(i);
						}	
						if(modelPartides.getSize() > 0){			
							for(Iterator it = p1.iterator(); it.hasNext();){//Agaf totes ses tirades i les pos a sa taula
								t1 = (Tirada)it.next();//Pos a cada fila s'informacio de sa tirada
								Object[] informacio = {t1.getNumUsuari(), t1.getBenColocats(), t1.getMalColocats(), ((llistaPartides.getSelectedValue().getDificultat() == 1)? t1.ajuda() : "")};
								modelTirades.addRow(informacio);
							}
						}
						
						//Si sa partida esta acabada desactiv es boto i es jtextfield
						if(p1.getAcabada()){
							tEntrada.setEnabled(false);
							bEntrada.setEnabled(false);
						}
						else{
							tEntrada.setEnabled(true);
							bEntrada.setEnabled(true);
						}
						//Amb sa partida anterior calcular sa diferencia i despres que sigui sa nova partida sa partida anterior
						pausarPartida();
						guardar(false);
						partidaAnterior = p1;
					}
				}
			}
		});
		//FALTA CALCULAR SA DIFERENCIA SI NO HA ACABAT QUANT CANVIES DE PARTIDA I REANUDAR S'HORA
		//ENTRADA DE TIRADES
		bEntrada.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				insertarTirada();
			}
		});
	
		menuNovaSenseAjuda.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				novaPartidaIActualitzar(2);
			}
		});
		
		menuNovaAmbAjuda.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				novaPartidaIActualitzar(1);
			}
		});
		
		menuOnline.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(partidaAnterior != null){
					partidaAnterior.calcularDiferencia();
					guardar(true);//Es sa partida anterior a s'altre lloc es contrari
				}
				partidaAnterior = null;
				//Quant es selecciona es boto d'online carregar partides de sa bd
				//Lev totes tirades i actualitz partides
				actualitzarPartides();
				if(objJoc.numeroPartides() == 0){
					tEntrada.setEnabled(false);
					bEntrada.setEnabled(false);
				}
				llistaPartides.setSelectedIndex(0);
			}
		});
		
		tEntrada.addKeyListener(new KeyAdapter(){
			public void keyReleased(KeyEvent e){
				//Si sa tecla pitjada es intro i esta ben format insertar tirada
				if(e.getExtendedKeyCode() == 10){
					if(!(tEntrada.getText().isEmpty())){
						insertarTirada();
					}
				}
			}
		});
		
		rankingOnline.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				ranking.mostrarRanking();
			}
		});
		
		borrarPartides.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Arxiu.arxiuPartides.delete();
				actualitzarPartides();
			}
		});
		
		//Seleccion sa primera partida, si el posa mes amunt no surten les tirades
		llistaPartides.setSelectedIndex(0);
		
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setVisible(true);
		//~ System.out.println(modelTirades.isCellEditable(0, 0));//Mostra si ses cel·les son editables
	}
	private class TancarMasterMind extends WindowAdapter{
		public void windowClosing(WindowEvent e){
			pausarPartida();
			guardar(false);
			System.exit(0);
		}
	}
	private void guardar(boolean contrari){
		boolean res = menuOnline.isSelected();
		if(contrari) res = !res;
		if(res){//Si es checkbox està seleccionat guardar ses partides a sa BD sino a nes fitxer
			Singleton.getInstancia().codiGuardar(2);
			if(partidaAnterior != null){
				Singleton.getInstancia().getMagatzemGuardar().guardar(partidaAnterior);
			}
		}
		else{
			Singleton.getInstancia().codiGuardar(1);//Guard totes les partides al fitxer perque sino surt la capçalera
			for(Iterator it = objJoc.iterator(); it.hasNext();){
				Singleton.getInstancia().getMagatzemGuardar().guardar((Partida)it.next());
			}
		}
		Singleton.getInstancia().getMagatzemGuardar().close();//Necessit es close perque sino no me deixarà eliminar el fitxer de partides perque hi haura stream obert pero a sa bd no fara res
		//Pero necessit posar-ho a s'interficie perque sino no ho trobarà, ja que es creat a partir de s'interficie
	}
	private void pausarPartida(){
		if(partidaAnterior != null && !partidaAnterior.getAcabada()){//Si no esta acabada calcular temps
			partidaAnterior.calcularDiferencia();
		}
	}
	private void novaPartidaIActualitzar(int dificultat){
		objJoc.novaPartida(dificultat, frameUsuari.getUsuari());
		modelPartides.addElement(objJoc.getPartida(objJoc.numeroPartides()-1));
		llistaPartides.setSelectedIndex(modelPartides.size()-1);
		panScrollPartides.getVerticalScrollBar().setValue(panScrollPartides.getVerticalScrollBar().getMaximum());
		guardar(false);
	}
	private void insertarTirada(){
		String infoTirada = tEntrada.getText().trim();
		tEntrada.setText("");
		Tirada t1 = null;
		Partida select = llistaPartides.getSelectedValue();
		//Comprov si s'entrada es correcte
		if(objJoc.benFormat(infoTirada) && !select.getAcabada()){//Fer tirada i actualitzar sa taula de tirades
			t1 = new Tirada(infoTirada);
			select.ferTirada(t1);
			Object[] informacio = {t1.getNumUsuari(), t1.getBenColocats(), t1.getMalColocats(), ((select.getDificultat() == 1)? t1.ajuda() : "")};
			modelTirades.addRow(informacio);
		}
		//Despres d'insertar sa tirada mir si sa partida esta acabada
		if(select.getAcabada()){
			//Que no pugui insertar mes tirades
			tEntrada.setEnabled(false);
			bEntrada.setEnabled(false);
			//Missatge d'enhorabona
			JOptionPane.showMessageDialog(null, "Enhorabona has acabat sa partida!", "Partida completada", JOptionPane.PLAIN_MESSAGE);
		}
		else{
			//Afegesc tirada i calcul temps
			select.calcularDiferencia();
			//Despres de fer una tirada guardar sa tirada
			select.reanudarPartida();
			tEntrada.setEnabled(true);
			bEntrada.setEnabled(true);
		}
		guardar(false);
		actualitzarPartides();
		panScrollTirades.getVerticalScrollBar().setValue(panScrollTirades.getVerticalScrollBar().getMaximum());//No me fa scroll be
		tEntrada.requestFocus();
	}
	private void actualitzarPartides(){
		int index = llistaPartides.getSelectedIndex();
		for(int i = modelTirades.getRowCount()-1; i >= 0; i-- ){
			modelTirades.removeRow(i);
		}
		if(menuOnline.isSelected()){//Carreg ses partides de sa bd o des fitxer
			objJoc.online(2, frameUsuari.getUsuari());
		}
		else{
			objJoc.online(1, frameUsuari.getUsuari());
		}
		//~ System.out.println("ROWS " + objJoc.numeroPartides());
		modelPartides = new DefaultListModel<Partida>();
		llistaPartides.setModel(modelPartides);
		for(int x = 0; x < objJoc.numeroPartides(); x++){
			//~ System.out.println(objJoc.getPartida(objJoc.numeroPartides()-1).getUsuari() + ">>" + frameUsuari.getUsuari());
			if(objJoc.getPartida(x).getUsuari().equals(frameUsuari.getUsuari())){
				modelPartides.addElement(objJoc.getPartida(x));
			}
		}
		llistaPartides.setSelectedIndex(index);
	}
	public static void main(String[] args){
		new JocGrafic();
	}
}

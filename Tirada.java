import java.util.*;
import java.io.Serializable;

class Tirada implements Serializable{
	//Atributs
	private byte[] numUsuari = new byte[5];
	private byte[] numMal = new byte[5];
	private byte malColocats = 0;
	private byte benColocats = 0;
	public Tirada(String num){
		for(int x = 0; x < 5; x++){
			numUsuari[x] = (byte)(num.charAt(x)-48);
		}
	}
	public Tirada(String num, String numMal, byte malColocats, byte benColocats){//String numUsuari, String numMal, byte malColocats, byte benColocats
		for(int x = 0; x < 5; x++){
			this.numUsuari[x] = (byte)(num.charAt(x)-48);
		}
		for(int x = 0; x < 5; x++){
			this.numMal[x] = (byte)(numMal.charAt(x)-48);
		}
		this.malColocats = malColocats;
		this.benColocats = benColocats;
	}
	//Getters i setters
	public byte getBenColocats(){
		return benColocats;
	}
	public byte getMalColocats(){
		return malColocats;
	}
	public String getNumUsuari(){
		String numTirada = "";
		for(int x = 0; x < numUsuari.length; x++){
			numTirada += numUsuari[x];
		}
		return numTirada;
	}
	public byte[] getNumMal(){return numMal;}
	public String getNumMalString(){
		String resultat = "";
		for(int x = 0; x < numMal.length; x++){
			resultat += numMal[x];
		}
		return resultat;
	}
	//Altres metodes
	public String ajuda(){
		String resultat = "";
		for(int x = 0; x < getNumMal().length; x++){
			resultat += getNumMal()[x];
		}
		return resultat;
	}
	public void colocats(byte[] numCpu){
		
		//Ben colocats
		for( byte x = 0; x < 5; x++ ){
			if( numCpu[x] == numUsuari[x] ){
				numMal[x] = 1;
				benColocats++;
			}
		}
	}
	public void malColocats(byte[] numCpu){
		/* 0 vol dir que res
		 * 1 vol dir colocat
		 * 2 vol dir malposicionat*/
		 boolean seguir;
		 
		
		for(byte x = 0; x < 5; x++){
			if(numMal[x] != 1){//Si no esta ben posicionat
				seguir = true;
				for(byte y = 0; y < 5 && seguir; y++){
					if(numMal[y] == 0 && numUsuari[y] == numCpu[x]){//Si son iguals i no s'ha posicionat encara
						malColocats++;
						numMal[y] = 2;
						seguir = false;
					}
				}
			}
		}
	}
	//Metodes sobrescrits
	public String toString(){
		String cad = "";
		for(int x = 4; x >= 0; x--){
			cad = numUsuari[x] + cad;
		}
		return ("Numero " + cad + " te " + benColocats + " numeros ben colocats i " + malColocats + " numeros que no estan ben colocats");
	}
}

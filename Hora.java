import java.util.*;
import java.io.Serializable;

class Hora implements Serializable{
	private byte hora = 0;
	private byte minut = 0;
	private byte segon = 0;
	private short milisegon = 0;
	private GregorianCalendar sessio;
	
	//Es crea una Hora quant es comença i quant es surt de l'aplicació o s'acaba es guarda la diferencia en hora, minut i segon
	//Perque quant es guard el GregorianCalendar dins un fitxer i l'obres un altre dia segueix contant
	
	public Hora(){
		sessio = new GregorianCalendar();
	}
	public Hora(Hora arg){
		this.hora = arg.getHora();
		this.minut = arg.getMinuts();
		this.segon = arg.getSegons();
		this.milisegon = arg.getMilisegons();
	}
	public Hora(int hora, int min, int seg, int ms){
		this.hora = (byte)hora;
		this.minut = (byte)min;
		this.segon = (byte)seg;
		this.milisegon = (short)ms;
	}
	public Hora clonar(){
		return new Hora(this);
	}
	public byte getHora(){
		return hora;
	}
	public byte getMinuts(){
		return minut;
	}
	public byte getSegons(){
		return segon;
	}
	public short getMilisegons(){
		return milisegon;
	}
	private int getHoraSessio(){
		return sessio.get(Calendar.HOUR_OF_DAY);
	}
	private int getMinutSessio(){
		return sessio.get(Calendar.MINUTE);
	}
	private int getSegonSessio(){
		return sessio.get(Calendar.SECOND);
	}
	private int getMiliSessio(){
		return sessio.get(Calendar.MILLISECOND);
	}
	public void calcularDiferencia(){
		if(sessio != null){
			Hora fi = new Hora();
			//Faig sa diferencia en milisegons
			int res = ((fi.getHoraSessio()*3600000) + (fi.getMinutSessio()*60000) + (fi.getSegonSessio()*1000) + fi.getMiliSessio())-((this.getHoraSessio()*3600000) + (this.getMinutSessio()*60000) + (this.getSegonSessio()*1000) + this.getMiliSessio());
			res += (this.hora*3600000)+(this.minut*60000)+(this.segon*1000)+this.milisegon;//Agaf s'hora i tot lo que hi havia antes ho pas a ms i despres a hores, minuts...
			//ho pas a hores, minuts i segons
			int hora = (res/3600000);
			int minuts = ((res-(hora*3600000))/60000);
			int segons = ((res-(hora*3600000)-(minuts*60000))/1000);
			int mili = (res-(hora*3600000)-(minuts*60000)-(segons*1000));
			
			this.hora = (byte)hora;
			this.minut = (byte)minuts;
			this.segon = (byte)segons;
			this.milisegon = (short)mili;
			sessio = null;
		}
	}
	public void seguir(){//Per continuar si no ha acabat
		sessio = new GregorianCalendar();
	}
	private String ferAfegit(byte arg){
		String resultat = String.valueOf(arg);
		if(arg < 10 && arg >= 0){
			resultat = "0" + arg;
		}
		return resultat;
	}
	public boolean equals(Object arg){
		if(this == arg){
			return true;
		}
		if(arg == null){
			return false;
		}
		if(arg instanceof Hora){
			Hora obj = (Hora)arg;
			return (this.getHora() == obj.getHora() && this.getMinuts() == obj.getMinuts() && this.getSegons() == obj.getSegons() && this.getMilisegons() == obj.getMilisegons());
		}
		return false;
	}
	public String toString(){
		String resultat = ferAfegit(this.getHora()) + ":" + ferAfegit(this.getMinuts()) + ":" + ferAfegit(this.getSegons()) + ":" + this.getMilisegons();
		return resultat;
	}
	
}

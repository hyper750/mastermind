import java.io.*;
import java.util.*;
import javax.swing.*;
import java.awt.*;

class Joc{
	public static final File arxiuPartides = new File("Partides.obj");
	
	private Partida partida = null;
	private Tirada tirada = null;
	private byte dificultat;
	private boolean seguirJugant = true;
	private ArrayList<Partida> partides;
	
	
	public Joc(String usuari){
		//Carreg partides del fitxer per defecte
		online(1, usuari);
	}
	public Iterator iterator(){
		return partides.iterator();
	}
	public void novaPartida(int dificultat, String usuari){
		partides.add(new Partida(dificultat, usuari));
	}
	public int numeroPartides(){
		return partides.size();
	}
	public Partida getPartida(int index){
		return partides.get(index);
	}
	public boolean benFormat(String cadUsu){//Mira si el numero de l'usuari esta ben format
		for(byte x = 0; x < cadUsu.length(); x++){
			if( !((int)cadUsu.charAt(x) >= 48 && (int)cadUsu.charAt(x) <= 57 || (int)cadUsu.charAt(x) == 45) ){
				JOptionPane.showMessageDialog(null, "Nomes poden ser numeros", "Error en la tirada", JOptionPane.ERROR_MESSAGE);
				return false;
			}
		}
		if( cadUsu.length() != 5 ){
			JOptionPane.showMessageDialog(null, "El numero te que ser de 5 xifres", "Error en la tirada", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		else if( cadUsu.charAt(0) == '-' ){
			JOptionPane.showMessageDialog(null, "No s'accepten numeros negatius", "Error en la tirada", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}
	public void online(int num, String usuari){
		int iid;
		Singleton.getInstancia().codiCarregar(num);
		partides = Singleton.getInstancia().getMagatzemCarregar().carregar(usuari);
		if(partides.size() > 0){
			iid = partides.get(partides.size()-1).getId();
		}
		else{
			iid = 0;
		}
		Partida.setId(iid);
	}
	/*private static String mostrarPartides(ArrayList<Partida> partides){
		String resultat = "";
		Partida partida1 = null;
		for(Iterator<Partida> it = partides.iterator(); it.hasNext();){
			partida1 = it.next();	//Si esta acabada la partida te dona el numero de l'ultima tirada ja que seria el numero aleatori endivinat
			resultat += partida1 + ((partida1.getAcabada())? "\nNumero aleatori: " + partida1.getTirada((partida1.getIntents()-1)).getNumUsuari() : "") + "\n\n";
		}
		return resultat;
	}
	
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		String numero = "";
		Partida partida = null;
		Tirada tirada = null;
		byte dificultat;
		boolean seguirJugant = true;
		Singleton.getInstancia().codiCarregar(1);
		ArrayList<Partida> partides = Singleton.getInstancia().getMagatzemCarregar().carregar();//Carregar partides i agafar id apartir des nombre de partides
		System.out.println(mostrarPartides(partides));
		new Partida().setId(partides.size());
		
		System.out.println( "|_____________________________________________________________________________|" );
		System.out.println( "|_________________________________MASTER_MIND_________________________________|" );
		System.out.println( "|_____________________________________________________________________________|" );
		
		while(seguirJugant){
			System.out.println("Nivell:");//Seleccionar nivell
			System.out.println("1. Principiant");//Amb sa taula de numeros 11122
			System.out.println("2. Avançat");
			do{
				dificultat = scan.nextByte();
			}while( !(dificultat >= 1 && dificultat <= 2) );
			
			partida = new Partida(dificultat);
			
			while( !partida.getAcabada() ){//Metres no acabi sa partida demanar numeros
				//Mostrar numero aleatori
				System.out.print("\n\nAleatori: " + partida.getNumCpu() + "\n");
				//Fer tirada
				do{
					System.out.println( "Inserta un numero" );
					numero = scan.next();
				}while(!benFormat(numero));//Si el numero es correcte fer tirada sino tornar a demanar
				partida.ferTirada(new Tirada(numero));
				//Mostrar tirades entrades
				System.out.println("ENTRADA  " + "  BEN_POS  " + "  MAL_POS  " + ((partida.getDificultat() == 1)? "  TAULA  " : "") );
				for(Iterator it = partida.iterator(); it.hasNext();){
					tirada = (Tirada)it.next();
					System.out.print( tirada.getNumUsuari() + "         " + tirada.getBenColocats() + "          " + tirada.getMalColocats() + "       ");
					
					for(int x = 0; x < tirada.getNumMal().length && dificultat == 1; x++){//Mostrar taula de 0, 1 i 2 si esta en principiant
						System.out.print(tirada.getNumMal()[x]);
					}
					System.out.println();
				}
				System.out.println("\n");
			}
			System.out.println("\n\nInformació partida: \n" + partida);
			//Partida acabada afegir a sa llista de partides
			partides.add(partida);
			System.out.println("Vols seguir jugant? n/s");
			if( !(scan.next().equalsIgnoreCase("s")) ){
				seguirJugant = false;
			}
		}
		
		//~ System.out.println("TAMANY: " + partides.size());//Per saber si carrega be les partides
		Singleton.getInstancia().codiGuardar(1);//De quina forma guardar les partides, 1. Fitxer 2. BD
		System.out.println(Singleton.getInstancia().getMagatzemGuardar());//Missatge de on es guarden les partides
		for(Iterator<Partida> it = partides.iterator(); it.hasNext();){//Escriure totes les partides al fitxer, me sembla un poc aixi escriure totes les partides de nou i no la ultima, pero sino surt la capçalera i dona error si troba dues capçaleres
			Singleton.getInstancia().getMagatzemGuardar().guardar(it.next());
		}
		scan.close();
	}*/
}

create database mastermind;
use mastermind;

create table partides(
	id int not null,
	usuari varchar(50) not null,
	hora tinyint not null,
	minut tinyint not null,
	segon tinyint not null,
	milisegon smallint not null,
	dificultat tinyint not null,
	numCpu char(5) not null,
	acabada boolean not null,
	primary key (id, usuari)
);

create table tirades(
	idTirada int unsigned auto_increment primary key,
	usuari varchar(50) not null,
	idPartida int not null references partides(id) on update no action on delete cascade,
	numUsuari char(5) not null,
	numMal char(5) not null,
	malColocats tinyint not null,
	benColocats tinyint not null
);
-- Amb ajuda
create table rankingfacil(
	pos int primary key,
	idPartida int not null,
	usuari varchar(50) not null,
	tirades int not null,
	hora tinyint not null,
	minut tinyint not null,
	segon tinyint not null,
	milisegon smallint not null
);
-- Sense ajuda
create table rankingdificil(
	pos int primary key,
	idPartida int not null,
	usuari varchar(50) not null,
	tirades int not null,
	hora tinyint not null,
	minut tinyint not null,
	segon tinyint not null,
	milisegon smallint not null
);

delimiter $
drop trigger if exists ranking$
create trigger ranking after update on partides
for each row
begin
	declare eof int default 0;
	declare cont int default 1;
	declare resid int;
	declare resusuari varchar(50);
	declare reshora tinyint;
	declare resminut tinyint;
	declare ressegon tinyint;
	declare resmilisegon smallint;
	declare restirades int;
	
	declare recorrePartides cursor for select id, usuari, hora, minut, segon, milisegon, (select count(*) from tirades where tirades.idPartida=partides.id and tirades.usuari=partides.usuari) from partides where dificultat = new.dificultat and acabada = 1 order by hora asc, minut asc, segon asc, milisegon asc, 7 asc;
	declare continue handler for not found set eof = 1;
	
	if(new.dificultat = 1) then
		delete from rankingfacil;
	else
		delete from rankingdificil;
	end if;
	
	open recorrepartides;
		1_part : loop
			fetch recorrepartides into resid, resusuari, reshora, resminut, ressegon, resmilisegon, restirades;
			if(eof = 1) then
				leave 1_part;
			end if;
			if(new.dificultat = 1) then
				insert into rankingfacil values (cont, resid, resusuari, restirades, reshora, resminut, ressegon, resmilisegon);
			else
				insert into rankingdificil values (cont, resid, resusuari, restirades, reshora, resminut, ressegon, resmilisegon);
			end if;
			
			set cont = cont + 1;
		end loop 1_part;
	close recorrepartides;
end$
delimiter ;

-- call insertarpartida('" + partida.getId() + "', '" + partida.getUsuari() + "', , '" + partida.getHora().getHora() + "', '" + partida.getHora().getMinuts() + "', '" + partida.getHora().getSegons() + "', '" + partida.getHora().getMilisegons() + "', '" + partida.getDificultat() + "', '" + partida.getNumCpu() + "', '" + partida.getAcabada() + "'";
delimiter $
drop procedure if exists insertarpartida$
create procedure insertarpartida(in eid int, in eusuari varchar(255), in ehora tinyint, in eminut tinyint, in esegon tinyint, in emilisegon smallint, in edificultat tinyint, in enumcpu char(5), in eacabada boolean)
begin
	if((select count(*) from partides where id=eid and usuari=eusuari) = 0) then
		insert into partides values(eid, eusuari, ehora, eminut, esegon, emilisegon, edificultat, enumcpu, eacabada);
	else-- update = "update partides set hora='" + partida.getHora().getHora() + "', minut='" + partida.getHora().getMinuts() + "', segon='" + partida.getHora().getSegons() + "', milisegon='" + partida.getHora().getMilisegons() + "', acabada='1' where id='" + partida.getId() + "' and usuari='" + partida.getUsuari() + "'";//Actualitzar temps, si esta acabada i ses tirades
		update partides set hora=ehora, minut=eminut, segon=esegon, milisegon=emilisegon, acabada=eacabada where usuari=eusuari and id=eid;
	end if;
end$
delimiter ;

create user 'mastermind'@'localhost' identified by '123Cicle123';
create user 'mastermind'@'%' identified by '123Cicle123';
grant insert, select on mastermind.tirades to 'mastermind'@'localhost', 'mastermind'@'%';
grant select on mastermind.partides to 'mastermind'@'localhost', 'mastermind'@'%';
grant execute on procedure mastermind.insertarpartida to 'mastermind'@'localhost', 'mastermind'@'%';
grant select on mastermind.rankingfacil to 'mastermind'@'localhost', 'mastermind'@'%';
grant select on mastermind.rankingdificil to 'mastermind'@'localhost', 'mastermind'@'%';

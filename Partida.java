import java.util.*;
import java.io.Serializable;


class Partida implements Serializable, Comparable<Partida>{
	//Atributs
	private static int contId;
	final private Hora temps;
	final private int id;
	private byte dificultat;//Guard dificultat per quant ho carregui
	private ArrayList<Tirada> tirades = new ArrayList<Tirada>();
	final private byte[] numCpu = new byte[5];
	private boolean acabada = false;
	private String usuari;
	//Constructors
	public Partida(){
		int numRandom = new Random().nextInt(100000);//De 0-99999
		for(int y = 4; y >= 0; y--){//Desmont es nombre i apart posa 0 si el nombre es mes petit que 5
			numCpu[y] = (byte)(numRandom % 10);
			numRandom /= 10;
		}
		this.dificultat = 1;
		contId++;
		id = contId;
		temps = new Hora();
	}
	public Partida(int dificultat, String usuari){
		int numRandom = new Random().nextInt(100000);//De 0-99999
		for(int y = 4; y >= 0; y--){//Desmont es nombre i apart posa 0 si el nombre es mes petit que 5
			numCpu[y] = (byte)(numRandom % 10);
			numRandom /= 10;
		}
		this.dificultat = (byte)dificultat;
		contId++;
		id = contId;
		this.usuari = usuari;
		temps = new Hora();
	}
	public Partida(int id, String numcpu, int dificultat, int hora, int minut, int segon, int ms, String usuari, boolean acabada){
		this.id = id;
		for(int x = 0; x < numCpu.length; x++){
			this.numCpu[x] = (byte)(numcpu.charAt(x)-48);
		}
		this.dificultat = (byte)dificultat;
		temps = new Hora(hora, minut, segon, ms);
		this.usuari = usuari;
		this.acabada = acabada;
	}
	//Getters i Setters
	public Hora getHora(){//Retorn una copia
		return this.temps;
	}
	public String getUsuari(){return usuari;}
	public int getId(){return id;}
	public String getNumCpu(){//No m'agrada que es pugui veure es numero aleatori
		String numero = "";
		for(int x = 0; x < numCpu.length; x++){
			numero += numCpu[x];
		}
		
		return numero;
	}
	public void afegirTirada(String numUsuari, String numMal, byte malColocats, byte benColocats){
		tirades.add(new Tirada(numUsuari, numMal, malColocats, benColocats));
	}
	public boolean getAcabada(){return acabada;}//Si estan acabades d'un colo o marcades de colcar manera i ses altres d'un altre colo i permiti que pugui continuar
	public int getIntents(){return tirades.size();}
	public Tirada getTirada(int index){return tirades.get(index);}
	public byte getDificultat(){return dificultat;}
	public static void setId(int argId){
		contId = argId;
	}
	//Altres metodes
	public void calcularDiferencia(){
		temps.calcularDiferencia();
	}
	public void reanudarPartida(){
		temps.seguir();
	}
	public Iterator iterator(){
		return tirades.iterator();
	}
	public void ferTirada(Tirada t){//Fer una tirada ben formada
		tirades.add(t);
		t.colocats(numCpu);//Calcula numero ben posicionats d'aquesta tirada  segons el numero random de la partida
		t.malColocats(numCpu);//Calcula numero mal posicionats d'aquesta tirada segons el numero random de la partida
		if(t.getNumUsuari().equals(getNumCpu())){
			acabada = true;
			temps.calcularDiferencia();
		}
	}
	//Metodes sobrescrits
	public boolean equals(Object obj){
		if(this == obj) return true;
		if(obj == null) return false;
		if(obj instanceof Partida){
			return this.getId() == ((Partida)obj).getId();
		}
		return false;
	}
	public int compareTo(Partida obj){
		if(this.getId() == obj.getId()) return 0;
		if(this.getId() > obj.getId()) return 1;
		return -1;
	}
	public String toString(){
		return ("ID partida: " + getId() + " Temps: " + temps + " Numero d'intents " + tirades.size() + ((getAcabada())? " Partida acabada" : " Partida no acabada"));
	}
}

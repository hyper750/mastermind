import javax.swing.*;
import java.awt.*;
import javax.swing.event.*;
import java.awt.event.*;
import javax.swing.table.*;
import java.sql.*;

class Ranking{
	private JFrame rankingFrame = new JFrame("Ranking");
	private JPanel pan = (JPanel)rankingFrame.getContentPane();
	private JTabbedPane tabpan = new JTabbedPane();
	
	private DefaultTableModel modelPartides = null;
	private JTable taulaPartides = null;
	private JScrollPane scrollFacil = null;
	private Object[][] informacioTaula = {};
	
	private DefaultTableModel modelPartidesDificil = null;
	private JTable taulaPartidesDificil = null;
	private JScrollPane scrollDificil = null;
	private Object[][] informacioTaulaDificil = {};
	
	private String[] nomColumnes = {"Posici�", "Usuari", "Tirades", "Hora", "Minut", "Segon", "Milisegon"};
	
	public Ranking(){//Fer una taula amb es ranking, amb un trigger i que surti com a nova pestanya quant fas clic a ranking a nes menu
		rankingFrame.setSize(600, 400);
		JPanel panrankingfacil = new JPanel(new BorderLayout());
		JPanel panrankingdificil = new JPanel(new BorderLayout());
		
		//Taula facil
		modelPartides = new DefaultTableModel(informacioTaula, nomColumnes){
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};
		taulaPartides = new JTable(modelPartides);
		scrollFacil = new JScrollPane(taulaPartides);
		taulaPartides.setRowHeight(30);
		JButton bActualitzarFacil = new JButton(new ImageIcon("img/arrow_50x50.png"));
		bActualitzarFacil.setBorder(BorderFactory.createEmptyBorder());//Llev bordes
		bActualitzarFacil.setFocusPainted(false);
		bActualitzarFacil.setBackground(new Color(238, 238, 238));
		/*DefaultTableCellRenderer centreRenderFacil = new DefaultTableCellRenderer();
		centreRenderFacil.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
		for(int x = 0; x < 7; x++){
			taulaPartides.getColumn(taulaPartides.getColumnName(x)).setCellRenderer(centreRenderFacil);
		}*/
		taulaPartides.getTableHeader().setReorderingAllowed(false);
		
		//Taula dificil
		modelPartidesDificil = new DefaultTableModel(informacioTaulaDificil, nomColumnes){
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};
		taulaPartidesDificil = new JTable(modelPartidesDificil);
		scrollDificil = new JScrollPane(taulaPartidesDificil);
		taulaPartidesDificil.setRowHeight(30);
		/*for(int x = 0; x < 7; x++){
			taulaPartidesDificil.getColumn(taulaPartidesDificil.getColumnName(x)).setCellRenderer(centreRenderFacil);
		}*/
		taulaPartidesDificil.getTableHeader().setReorderingAllowed(false);
		JButton bActualitzarDificil = new JButton(new ImageIcon("img/arrow_50x50.png"));
		bActualitzarDificil.setBorder(BorderFactory.createEmptyBorder());//Llev bordes
		bActualitzarDificil.setFocusPainted(false);
		bActualitzarDificil.setBackground(new Color(238, 238, 238));
		
		panrankingfacil.add(bActualitzarFacil, BorderLayout.SOUTH);
		panrankingfacil.add(scrollFacil, BorderLayout.CENTER);
		panrankingdificil.add(bActualitzarDificil, BorderLayout.SOUTH);
		panrankingdificil.add(scrollDificil, BorderLayout.CENTER);
		
		tabpan.add(panrankingfacil, "Ranking Facil");
		tabpan.add(panrankingdificil, "Ranking Dificil");
		pan.add(tabpan);
		
		//Quant es faixi clic a sa pestanya es faixi sa consulta
		
		tabpan.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(tabpan.getSelectedIndex() == 0){
					//Facil
					actualitzarFacil();
				}
				else{
					//Dificil
					actualitzarDificil();
				}
			}
		});
		
		bActualitzarFacil.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				actualitzarFacil();
			}
		});
		
		bActualitzarDificil.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				actualitzarDificil();
			}
		});
		rankingFrame.addWindowListener(new WindowAdapter(){
			public void windowClosed(){
				rankingFrame.setVisible(false);
			}
		});
		
		rankingFrame.setLocationRelativeTo(null);
		rankingFrame.setVisible(false);
	}
	
	public void mostrarRanking(){
		//seleccionar ranking de sa BD segons es nivell
		actualitzarFacil();
		rankingFrame.setLocationRelativeTo(null);
		rankingFrame.setVisible(true);
	}
	private static void mostraSQLException(SQLException ex){
		ex.printStackTrace(System.err);
		System.err.println("SQLState: " + ex.getSQLState());
		System.err.println("Error Code: " + ex.getErrorCode());
		System.err.println("Message: " + ex.getMessage());
		Throwable t = ex.getCause();
		while(t != null){
			System.out.println("Cause: " + t);
			t = t.getCause();
		}
	}
	private void actualitzarFacil(){
		informacioTaula = infoBD(1);
		modelPartides.setDataVector(informacioTaula, nomColumnes);
	}
	private void actualitzarDificil(){
		informacioTaulaDificil = infoBD(2);
		modelPartidesDificil.setDataVector(informacioTaulaDificil, nomColumnes);
	}
	private Object[][] infoBD(int num){
		Object[][] files = null;
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		String url = "jdbc:mysql://localhost:3306";
		String user = "mastermind";
		String pwd = "123Cicle123";
		String use = "use mastermind";
		String contfacil = "select count(*) from rankingfacil";
		String contdificil = "select count(*) from rankingdificil";
		int numFiles = 0;
		int contadorFiles = 0;
		String consultaFacil = "select pos, usuari, tirades, hora, minut, segon, milisegon from rankingfacil";
		String consultaDificil = "select pos, usuari, tirades, hora, minut, segon, milisegon from rankingdificil";
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(url, user, pwd);
			st = con.createStatement();
			st.executeUpdate(use);

			//Cada fila
			if(num == 1){
				rs = st.executeQuery(contfacil);
			}
			else if(num == 2){
				rs = st.executeQuery(contdificil);
			}
			rs.next();
			numFiles = rs.getInt(1);
			files = new Object[numFiles][7];
			if(num == 1){
				rs = st.executeQuery(consultaFacil);
			}
			else if(num == 2){
				rs = st.executeQuery(consultaDificil);
			}
			
			while(rs.next()){
				files[contadorFiles][0] = rs.getString(1);//Posicio
				files[contadorFiles][1] = rs.getString(2);//Usuari
				files[contadorFiles][2] = rs.getString(3);//Tirades
				files[contadorFiles][3] = rs.getString(4);//Hora
				files[contadorFiles][4] = rs.getString(5);//Minut
				files[contadorFiles][5] = rs.getString(6);//Segon
				files[contadorFiles][6] = rs.getString(7);//Ms
				contadorFiles++;
			}
			
		}
		catch(ClassNotFoundException e){
			e.printStackTrace();
		}
		catch(SQLException e){
			mostraSQLException(e);
		}
		
		
		return files;
	}
}
